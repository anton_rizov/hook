package com.galileev.hook;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.galileev.hook.Bind.bind;
import static com.galileev.hook.arguments.Instructions.rest;

public class Hook {
  private final String displayName;
  private final List<BoundInvocable> handlers = new CopyOnWriteArrayList<>();

  public Hook(String displayName) {
    this.displayName = displayName;
  }

  public Hook() {
    this.displayName = String.valueOf(System.identityHashCode(this));
  }

  public void run(Object... args) {
    runUntil(Condition.NEVER, args);
  }

  public Object runUntil(Condition c, Object... args) {
    for (BoundInvocable handler : handlers) {
      Object result = handle(handler, args);
      if (c.evaluate(result))
        return result;
    }
    return null;
  }

  public Object runUntil(Object o, Object... args) {
    return runUntil(new Condition.EQUAL(o), args);
  }

  public Object runSpecial(Object... args) {
    return runSpecialUntil(Condition.NEVER, args);
  }

  public Object runSpecialUntil(Condition c, Object... args) {
    Object result = null;

    Iterator<BoundInvocable> it = handlers.iterator();
    if (it.hasNext()) {
      result = handle(it.next(), args);
    }

    while (it.hasNext() && !c.evaluate(result)) {
      result = handle(it.next(), new Object[]{result});
    }

    return result;
  }

  public BoundInvocable addHandler(Object target) {
    Method[] declaredMethods = target.getClass().getDeclaredMethods();

    if (declaredMethods.length != 1)
      throw new IllegalArgumentException("Must have exactly 1 declared method");

    return addHandler(target, declaredMethods[0]);
  }

  public BoundInvocable addHandler(Object target, Method method) {
    return addHandler(bind(target, method, rest()));
  }

  public BoundInvocable addHandler(Object target, String methodName, Class<?>... args) throws NoSuchMethodException {
    return addHandler(target, target.getClass().getMethod(methodName, args));
  }

  public BoundInvocable addHandler(BoundInvocable handler) {
    handlers.add(handler);
    return handler;
  }

  public void removeHandler(BoundInvocable handler) {
    handlers.remove(handler);
  }

  @Override
  public String toString() {
    return "Hook {" + displayName + "; " + handlers + "}";
  }

  private static Object handle(BoundInvocable handler, Object[] args) {
    try {
      return handler.invoke(args);
    } catch (IllegalAccessException e) {
      return null;
    } catch (InvocationTargetException e) {
      return null;
    }
  }
}
