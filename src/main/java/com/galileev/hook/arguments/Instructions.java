package com.galileev.hook.arguments;

import com.galileev.hook.Condition;
import com.galileev.hook.Conditions;

public class Instructions {
  public static Object arg() {
    return args(1);
  }

  public static Object arg(final int n) {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.add(vm.get(n));
      }
    };
  }

  public static Object args(final int n) {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.copy(n);
      }
    };
  }

  public static Object rest(final Object object) {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.processRest(object);
      }
    };
  }

  public static Object rest() {
    return rest(arg());
  }

  public static Object skip() {
    return skip(1);
  }

  public static Object skip(final int n) {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.skip(n);
      }
    };
  }

  public static Object begin(final Object... instructions) {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.executeProgram(instructions);
      }
    };
  }

  public static Object pop() {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.pop();
      }
    };
  }

  public static Object either(final Condition c, final Object thenBranch, final Object... elseBranch) {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
        vm.execute(c.evaluate(vm.pop()) ? thenBranch : begin(elseBranch));
      }
    };
  }

  public static Object nop() {
    return new Instruction() {
      @Override
      public void execute(VM vm) {
      }
    };
  }

  public static Object either(final Condition c) {
    return begin(unless(c, pop()),
                 arg());
  }

  public static Object when(final Condition test, final Object... body) {
    return either(test,
                  body.length > 0 ? begin(body) : arg(-1),
                  nop());
  }

  public static Object unless(final Condition c, Object... body) {
    return when(Conditions.not(c), body);
  }
}
