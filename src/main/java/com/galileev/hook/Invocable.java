package com.galileev.hook;

import java.lang.reflect.InvocationTargetException;

public interface Invocable {
  Object invoke(Object obj, Object... args)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
}
