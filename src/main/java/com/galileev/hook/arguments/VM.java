package com.galileev.hook.arguments;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class VM {
  private Object[] input;
  private int inputPos;
  private Object[] output;
  private int outputPos;

  private Class<?> varArgsClass;
  private ArrayList<Object> varArgs;

  private Object[] instructions;

  public VM(Object[] instructions, Class<?>[] parameterTypes) {
    this.instructions = instructions;

    int length = parameterTypes.length;

    output = new Object[length];

    if (length > 0) {
      Class<?> c = parameterTypes[length - 1];
      if (c.isArray()) {
        varArgsClass = c.getComponentType();
        varArgs = new ArrayList<>();
      }
    }
  }

  public void skip() {
    inputPos++;
  }

  public void skip(int n) {
    while (n-- > 0) {
      skip();
    }
  }

  public void copy() {
    add(input[inputPos++]);
  }

  public void copy(int n) {
    while (n-- > 0) {
      copy();
    }
  }

  public void add(Object o) {
    if (outputPos == output.length - 1 && varArgs != null) {
      varArgs.add(o);
    }
    else {
      output[outputPos++] = o;
    }
  }

  public Object pop() {
    return input[inputPos++];
  }

  public Object get(int pos) {
    return input[pos >= 0 ? pos: inputPos + pos];
  }

  public Object[] builgArgs(Object[] input) {
    this.input = input;
    outputPos = 0;
    inputPos = 0;

    executeProgram(instructions);

    if (varArgsClass != null) {
      output[output.length - 1] = toArray(varArgsClass, varArgs);
      varArgs.clear();
    }

    return output;
  }

  public void processRest(Object instruction) {
    while (inputPos < input.length) {
      execute(instruction);
    }
  }

  public void doTimes(int n, Object instruction) {
    while (n-- > 0) {
      execute(instruction);
    }
  }

  public void executeProgram(Object[] instructions) {
    for (Object instruction : instructions) {
      execute(instruction);
    }
  }

  public void execute(Object instruction) {
    if (instruction instanceof Instruction) {
      ((Instruction) instruction).execute(this);
    } else {
      add(instruction);
    }
  }

  private static Object toArray(Class<?> klass, List<Object> list) {
    Object result = Array.newInstance(klass, list.size());
    int index = 0;
    for (Object element : list) {
      Array.set(result, index++, element);
    }
    return result;
  }
}
