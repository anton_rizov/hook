package com.galileev.hook;

public class Conditions {
  public static Condition eq(Object value) {
    return new Condition.EQ(value);
  }

  public static Condition equal(Object value) {
    return new Condition.EQUAL(value);
  }

  public static Condition and(Condition... components) {
    return new Condition.AND(components);
  }

  public static Condition or(Condition... components) {
    return new Condition.OR(components);
  }

  public static Condition not(Condition condition) {
    return new Condition.NOT(condition);
  }
}
