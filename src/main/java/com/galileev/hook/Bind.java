package com.galileev.hook;

import com.galileev.hook.arguments.VM;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Bind {
  public static Invocable bind(final Method method, final Object... program) {
    final VM vm = new VM(program, method.getParameterTypes());

    return new Invocable() {
      @Override
      public Object invoke(Object obj, Object... args)
          throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return method.invoke(obj, vm.builgArgs(args));
      }
    };
  }

  public static BoundInvocable bind(final Object object, final Method method, final Object... program) {
    return new BoundInvocable(object, bind(method, program));
  }
}
