package com.galileev.hook.arguments;

interface Instruction {
  void execute(VM vm);
}
