package com.galileev.hook;

import java.lang.reflect.InvocationTargetException;

public class BoundInvocable {
  private final Object object;
  private final Invocable invocable;

  public BoundInvocable(Object object, Invocable invocable) {
    this.object = object;
    this.invocable = invocable;
  }

  public Object invoke(Object... args) throws InvocationTargetException, IllegalAccessException {
    return invocable.invoke(object, args);
  }
}
