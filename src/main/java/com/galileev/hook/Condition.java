package com.galileev.hook;

public interface Condition {
  boolean evaluate(Object arg);

  Condition ALWAYS = new Condition() {
    @Override
    public boolean evaluate(Object arg) {
      return true;
    }
  };

  Condition NEVER = new Condition() {
    @Override
    public boolean evaluate(Object arg) {
      return false;
    }
  };

  class EQ implements Condition {
    private final Object value;

    public EQ(Object value) {
      this.value = value;
    }

    @Override
    public boolean evaluate(Object arg) {
      return value == arg;
    }
  }

  class EQUAL implements Condition {
    private final Object value;

    public EQUAL(Object value) {
      this.value = value;
    }

    @Override
    public boolean evaluate(Object arg) {
      return value == null ? arg == null : value.equals(arg);
    }
  }

  class NOT implements Condition {
    private final Condition actual;

    public NOT(Condition actual) {
      this.actual = actual;
    }

    @Override
    public boolean evaluate(Object arg) {
      return !actual.evaluate(arg);
    }
  }

  class AND implements Condition {
    private final Condition[] components;

    public AND(Condition... components) {
      this.components = components;
    }

    @Override
    public boolean evaluate(Object arg) {
      for (Condition component : components) {
        if (!component.evaluate(arg))
          return false;
      }
      return true;
    }
  }

  class OR implements Condition {
    private final Condition[] components;

    public OR(Condition... components) {
      this.components = components;
    }

    @Override
    public boolean evaluate(Object arg) {
      for (Condition component : components) {
        if (component.evaluate(arg))
          return true;
      }
      return false;
    }
  }
}
