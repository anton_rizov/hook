package com.galileev.hook;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.galileev.hook.Bind.bind;
import static com.galileev.hook.arguments.Instructions.*;
import static junit.framework.Assert.assertEquals;

public class BindTest {
  @Test
  public void allArguments() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method method = StringBuilder.class.getMethod("append", String.class);
    Invocable invocable = bind(method, "X");
    StringBuilder obj = new StringBuilder();
    invocable.invoke(obj);
    assertEquals("X", obj.toString());
  }

  @Test
  public void partial() throws Exception {
    Invocable invocable = bind(addMethod(), 3, arg());
    assertEquals(7, invocable.invoke(this, 4));
    assertEquals(5, invocable.invoke(this, 2));
  }

  @Test
  public void restArgs() throws Exception {
    Invocable m = bind(addMethod(), -2, rest());
    assertEquals(5, m.invoke(null, 1, 2, 3, 1));
    assertEquals(6, m.invoke(null, 8));
  }

  @Test
  public void ignoreArgs() throws Exception {
    Invocable invocable = bind(addMethod(), args(2));
    assertEquals(7, invocable.invoke(null, 3, 4, 3));
  }

  @Test
  public void skipArg() throws Exception {
    Invocable m = bind(addMethod(), 2, skip(), arg());
    assertEquals(3, m.invoke(null, 2, 1));
    assertEquals(3, m.invoke(null, 6, 1));
    assertEquals(3, m.invoke(null, 7, 1, 7, 7));
  }

  @Test
  public void testMaybe() throws Exception {
    Condition negative = new Condition() {
      @Override
      public boolean evaluate(Object arg) {
        return ((int) arg) < 0;
      }
    };
    Invocable m = bind(addMethod(), rest(when(negative)));
    assertEquals(-4, m.invoke(null, -1, 0, 2, -3));
    assertEquals(-1, m.invoke(null, -1));
    assertEquals(0, m.invoke(null));
    assertEquals(0, m.invoke(null, 1, 2, 3));
  }

  @Test
  public void testEither() throws Exception {
    Method append = StringBuilder.class.getMethod("append", String.class);
    Invocable m = bind(append, either(new Condition() {
      @Override
      public boolean evaluate(Object arg) {
        return ((int) arg) % 2 == 0;
      }
    }));

    StringBuilder builder = new StringBuilder();
    m.invoke(builder, 0, "x", "y");
    m.invoke(builder, 1, "x", "y");
    assertEquals("xy", builder.toString());
  }

  @Test
  public void testFullEither() throws Exception {
    Method append = StringBuilder.class.getMethod("append", String.class);
    Condition longEnough = new Condition() {
      @Override
      public boolean evaluate(Object arg) {
        return ((String) arg).length() > 3;
      }
    };
    Invocable m = bind(append, either(longEnough, arg(-1), "."));

    StringBuilder builder = new StringBuilder();
    m.invoke(builder, "xxx");
    m.invoke(builder, "x");
    m.invoke(builder, "xxxx");
    m.invoke(builder, "");
    m.invoke(builder, "");
    assertEquals("..xxxx..", builder.toString());
  }

  @Test
  public void subsetOfArgs() throws Exception {
    Invocable invocable = bind(addMethod(), skip(), skip(), args(2));
    assertEquals(7, invocable.invoke(null, 1, 2, 3, 4, 5, 6, 7, 8));
  }

  @Test
  public void variadicMethodNeedsArrayArgument() throws Exception {
    //noinspection PrimitiveArrayArgumentToVariableArgMethod
    addMethod().invoke(null, new int[]{1,2,3});
  }

  private Method addMethod() throws NoSuchMethodException {
    return this.getClass().getMethod("add", int[].class);
  }

  @SuppressWarnings("UnusedDeclaration")
  public static int add(int ...args) {
    int result = 0;
    for (int arg : args) {
      result += arg;
    }
    return result;
  }
}
