package com.galileev.hook;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HookTest {
  private StringBuilder out = new StringBuilder();
  private Hook hook = new Hook();

  @Test
  public void noReturn() {
    addVoidHandler("X");
    addVoidHandler("Y");

    hook.run();

    verify("XY");
  }

  @Test
  public void returnUntil() throws NoSuchMethodException {
    addHandler("X", true);
    addHandler("Y", false);
    addHandler("Z", true);

    hook.runUntil(false, "arg");

    verify("X(arg)Y(arg)");
  }

  @Test
  public void eachHandleReceivesArg() throws NoSuchMethodException {
    hook.addHandler(out, "append", String.class);
    hook.addHandler(out, "append", Object.class);
    hook.addHandler(out, "append", Object.class);

    hook.run(":)");

    verify(":):):)");
  }

  @Test
  public void runSpecial() {
    hook.addHandler(new Object() {
      public String handle(String a1, String a2) {
        return a1 + a2;
      }
    });
    hook.addHandler(new Object() {
      public String handle(String x) {
        return x + "1";
      }
    });
    hook.addHandler(new Object() {
      public String handle(String x) {
        return x + "2";
      }
    });
    assertEquals("abcd12", hook.runSpecial("ab", "cd"));
  }

  private void addHandler(String name, Object returnValue) {
    hook.addHandler(newHandler(name, returnValue));
  }

  private void addVoidHandler(final String name) {
    hook.addHandler(new Object() {
      public void handle() {
        out.append(name);
      }
    });
  }

  private Object newHandler(final String name, final Object returnValue) {
    return new Object() {
      public Object handle(Object arg) {
        out.append(name)
            .append("(")
            .append(arg)
            .append(")");

        return returnValue;
      }
    };
  }

  private void verify(String expected) {
    assertEquals(expected, out.toString());
  }
}
